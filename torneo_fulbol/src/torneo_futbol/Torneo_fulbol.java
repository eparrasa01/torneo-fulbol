
import static java.lang.System.out;
import java.util.Scanner;

/**
 *
 * @author USUARIO
 */
public class Torneo_fulbol {

   
     private static Scanner in = new Scanner(System.in);

    public static void main( String ... args ) {
        int numeroEquipos = readInt("Escriba el numero de equipos: ");
        torneo_futbol.Equipo [] equipos = new torneo_futbol.Equipo[numeroEquipos];
        for ( int i = 0 ; i < numeroEquipos ; i++ ) {
            equipos[i] = torneo_futbol.Equipo.creaEquipo(
                readString("\n\nNombre del equipo: "),
                readInt("Juego jugados: "),
                readInt("Juego ganados: "),
                readInt("Juego empatados: "),
                readInt("Juego perdidos: "),
                readInt("Goles a favor: "),
                readInt("Goles en contra: "));
        }
        for ( torneo_futbol.Equipo e : equipos ) {
            out.println( e );
        }
    }
    private static String readString( String message ) {
        System.out.print( message );
        return in.next();
    }
    private static int readInt(String message) {
        System.out.print(message);
        return in.nextInt();
    }
}